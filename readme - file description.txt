
CLIENTED

app.Practice
--contains the files for the middleware that connects to the backend

PracticeController - controller functions which talk to the functions in PracticeService, and return to the front end
PracticeService - layer between the controller and backend
GeneralService - a collection of functions that is used by several controllers

app.Repository.Client
-- contains files for the backend. Has functions that executes SQL commands using Eloquent for the client/practice table
-- the functions are declared in an interface and then implemented in the class

app.Repository.Users
-- contains files for the backend. Has functions that executes SQL commands using Eloquent for the querying the users table
-- the functions are declared in an interface and then implemented in the class

----------------------------------------------------------------------------------------------------------------

FUND ANALYTICS

Home
-- contains ReactJS files for different components used to render the main page of the site

funds-by-growth.php
-- queries the database to return information about growth of funds over 6mo, 1yr, 3yr etc.

funds-by-year.php
-- queries the database to return information about growth of funds over every year

Utils.php
-- some common functions use for querying and creating queries

FA_schema - shows how the tables in the database are connected with foreign keys

----------------------------------------------------------------------------------------------------------------

93andBeyond - http://[2607:fea8:1d5f:f213:b0ce:d75e:c493:9e2]/93andbeyond
