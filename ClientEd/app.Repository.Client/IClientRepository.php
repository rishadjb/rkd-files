<?php

namespace App\Repository\Client;
use Illuminate\Http\Request;


interface IClientRepository
{
    public function getAllClients();
    public function getClientListing($searchTerm);
    public function searchClientByName($name, $column = array('*'));
    public function searchClientByPhoneNumber($number, $column = array('*'));
    public function searchClientByFax($number, $column = array('*'));
    public function searchClientByEmail($email, $column = array('*'));
    public function getClientFromId($id);
    public function getClientByIframeToken($token);
    public function getClientWebsiteByClientId($token);

    public function saveClientDetails(Request $request);
}