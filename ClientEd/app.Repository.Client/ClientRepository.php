<?php
/**
 * Class ClientRepository
 * @package App\Repository\Client
 * @author Rishad J.B.
 */


namespace App\Repository\Client;

use App\Models\AddressRegion;
use App\Models\Country;
use App\Models\Language\Language;
use Illuminate\Http\Request;

use App\Models\Address;
use App\Models\Client;
use App\Models\ClientPhone;
use App\Models\ClientWebsite;
use App\Models\ClientLanguage;
use App\Models\ClientAccountType;
use App\Models\PMSoftware;
use App\Models\MaxUser;
use App\Models\HeaderType;

class ClientRepository implements IClientRepository
{
    public function getAllClients()
    {
        return Client::all();
    }
    
    public function getClientListing($searchTerm)
    {
        $searchTerm = $searchTerm == '_' ? '' : $searchTerm;

        return Client::select(
            Client::table().'.*',
            'clientPhone.phone',
            'clientPhone.extension',
            'countries.name as country',
            'provinces.name as province',
            'provinces.country_code as province_CC',
            'pmSoftware.name as pm_software')
                        ->leftjoin(ClientPhone::tableAs('clientPhone'), 'clientPhone.client_id', '=', Client::table().'.id')
                        ->like(Client::table().'.name', $searchTerm)
                        ->orLike('email', $searchTerm)
                        ->orLike('fax', $searchTerm)
                        ->orLike('phone', $searchTerm)
                        ->leftjoin(Address::tableAs('clientAddress'), 'clientAddress.client_id', '=', Client::table().'.id')
                        ->leftjoin(Country::tableAs('countries'), 'countries.code', '=', 'clientAddress.country_code')
                        ->leftjoin(AddressRegion::tableAs('provinces'), function($join)
                            {
                                $join->on('provinces.code', '=', 'clientAddress.region_code');
                                $join->on('provinces.country_code', '=', 'clientAddress.country_code');
                            })
                        ->leftjoin(PMSoftware::tableAs('pmSoftware'), 'pmSoftware.id', '=', Client::table().'.pm_software_id')
                        ->paginate(30);
    }

    public function getClientFromId($id)
    {
        $client =  Client::select(Client::table().'.*','clientAddress.*','clientWebsite.website','PMSoftware.name as pm_software')
            ->where(Client::table().'.id',$id)
            ->leftjoin(ClientWebsite::tableAs('clientWebsite'), 'clientWebsite.client_id', '=', Client::table().'.id')
            ->leftjoin(Address::tableAs('clientAddress'), 'clientAddress.client_id', '=', Client::table().'.id')
            ->leftjoin(PMSoftware::tableAs('PMSoftware'), 'PMSoftware.id', '=', Client::table().'.pm_software_id')
            ->get()
            ->first();

        //get client phones and push array object to client
        $phones = ClientPhone::where('client_id', '=', $client->id)->get();
        $client->phones = $phones;

        //get languages and push array object to client
        $languages = ClientLanguage::select('languages.id')
            ->where(ClientLanguage::table().'.client_id', $client->client_id)
            ->leftjoin(Language::tableAs('languages'), 'languages.id', '=', ClientLanguage::table().'.language_id')
            ->get();
        $client->language = $languages;

        return $client;
    }

    public function getClientByIframeToken($token)
    {
        return Client::select(Client::table().'.*')
            ->where(Client::table().'.iframe_token',$token)
            ->get()
            ->first();
    }

    public function getClientWebsiteByClientId($client_id)
    {
        $clientWebsiteRepository = new ClientWebsiteRepository();
        return $clientWebsiteRepository->getClientWebsiteFromClientId( $client_id );
    }

    public function searchClientByName($name, $column = array('*'))
    {
        return Client::select($column)
                        ->like('name', $name)
                        ->get();
    }

    public function searchClientByPhoneNumber($number, $column = array('*'))
    {
        return Client::select($column)
                    ->join(ClientPhone::tableAs('clientPhone'), 'clientPhone.client_id', '=', Client::table().'.id')
                    ->like(ClientPhone::table().'.phone', $number)
                    ->get();
    }

    public function searchClientByFax($number, $column = array('*'))
    {
        return Client::select($column)
                        ->like('fax', $number)
                        ->get();
    }

    public function searchClientByEmail($email, $column = array('*'))
    {
        return Client::select($column)
                        ->like('email', $email)
                        ->get();
    }

    public function saveClientDetails(Request $request)
    {

        //get the PMSoftware ID; new ID is returned is none exists
        $PMSoftware = PMSoftware::firstOrCreate([
            'name' => $request['pmSoftware']
        ]);

        //save the client info
        $client = Client::updateOrCreate(
            [
                'id' => $request['clientID']
            ],
            [
                'name' => $request['practiceName'],
                'email' => $request['email'],
                'fax' => $request['fax'],
                'region_type_id' => $request['region'],
                'pm_software_id' => $PMSoftware->id,
                'account_type_id' => $request['accountType'],
                'max_user_id' => $request['maxUsers'],
                'header_type_id' => $request['headerStyle'],
                'poc_id' => $request['pointOfContact'],
                'grey_scale' => $request['grayscale'] ? 1 : 0,
//                'logo' => $request['practiceName'],
//                'banner' => $request['practiceName'],
                'active' => $request['practiceStatus'],
            ]
        );


        Address::updateOrCreate(
            [
              'client_id' => $client->id
            ],
            [
                'line_1' => $request['address'],
                'city' => $request['city'],
                'postcode' => $request['postalCode'],
                'country_code' => $request['country'],
                'region_code' => $request['province']
            ]
        );

        //delete existing client phones
        ClientPhone::where('client_id', '=', $client->id)->delete();

        //add primary phone
        ClientPhone::updateOrCreate(
            [
                'client_id' => $client->id
            ],
            [
                'phone' => $request['phone'],
                'extension' => $request['phoneExt']
            ]
        );

        //add secondary phone is one is entered
        if($request['secondaryPhone'] != ''){
            ClientPhone::updateOrCreate(
                [
                    'client_id' => $client->id,
                    'phone' => $request['secondaryPhone'],
                ],
                [
                    'extension' => $request['phoneExt']
                ]
            );
        }


        ClientWebsite::updateOrCreate(
            [
                'client_id' => $client->id
            ],
            [
                'website' => $request['domain']
            ]
        );

        //get languages and push array object to client

        //delete existing languages for the client
        ClientLanguage::where('client_id', '=', $client->id)->delete();

        //get the new languages selected from the request
        $languages = explode(",", $request['language']);

        //insert the new language ids into the client_language table
        foreach($languages as $language_id){
            $client_language = new ClientLanguage;

            $client_language->client_id = $client->id;
            $client_language->language_id = $language_id;

            $client_language->save();
        }



        return $client;
    }


}