<?php
/**
 * General Service
 * This Library returns general information from the data
 * @author Rishad Bhattacharjee
 */

namespace App\Libraries\LifeLearn;

use App\Libraries\LifeLearn\BaseService as BaseService;


use Illuminate\Http\Request as Request;
use GuzzleHttp\Client as Client;

class GeneralService extends BaseService
{

    public static function getAccountTypeList()
    {		
        $items = array(
					(object) array("key"=>'1', "name"=>"Full"),
					(object) array("key"=>'2', "name"=>"Pending"),
					(object) array("key"=>'3', "name"=>"Web DVM")
				);
		
		return $items;
    }
    

    public static function getCountryList()
    {
		
		$client = new Client(['base_uri' => getenv('CE_SERVICE')]);
        $clientEdRequest = $client->request(
            'GET',
            "api/getAllCountries",
            [
                'headers' => [
                    'accept' => 'application/json'
                ]
            ]
        );
		$countryList = json_decode($clientEdRequest->getBody()->getContents());
		
		//change the array key from 'code' to 'key' so as to be handled correctly by custom
		foreach ( $countryList as $k=>$v )
		{
		  $countryList[$k]->key = $countryList[$k]->code;
		  unset($countryList[$k]->code);
		}
		
		return $countryList;
    }
    
	public static function getProvinceList($countryCode = '')
    {
		
		$provinceList = Utils::makeRequest('GET', "getAllProvincesByCountryCode/$countryCode");
		
		//change the array key from 'code' to 'key' so as to be handled correctly by custom
		foreach ( $provinceList as $k=>$v )
		{
		  $provinceList[$k]->key = $provinceList[$k]->code;
		  unset($provinceList[$k]->code);
		}
		
		return $provinceList;
    }
	
    public static function getRegionList()
    {
        $items = array(
					(object) array("key"=>'1', "name"=>"North America"),
					(object) array("key"=>'2', "name"=>"UK"),
					(object) array("key"=>'3', "name"=>"Aus/NZ")
				);
		return $items;
    }
	
    public static function getColorSchemes()
    {
        $items = array(
					(object) array("key"=>'CS1', "name"=>"Color Scheme 1"),
					(object) array("key"=>'CS2', "name"=>"Color Scheme 2")
				);
		return $items;
    }
	
    public static function getMaxUsers()
    {
        $items = array(
					(object) array("key"=>'1', "name"=>"1"),
					(object) array("key"=>'2', "name"=>"5"),
					(object) array("key"=>'3', "name"=>"15"),
					(object) array("key"=>'4', "name"=>"30"),
					(object) array("key"=>'5', "name"=>"50"),
					(object) array("key"=>'6', "name"=>"100"),
					(object) array("key"=>'7', "name"=>"200")
				);
		return $items;
    }
	
    public static function getPMSoftware()
    {
        $items = array(
					(object) array("key"=>'PMS1', "name"=>"PMS1"),
					(object) array("key"=>'PMS2', "name"=>"PMS2")
				);
		return $items;
    }
	
    public static function getLanguages($regionID)
    {
		
        $items = array(
					(object) array("key"=>'1', "name"=>"English"),
				);
		
		//for North America, add spanish
		if($regionID == 1){
			array_push($items, (object) array("key"=>'2', "name"=>"Spanish"));
		}
				
			
		return $items;
    }
	
    
	public static function getLanguageList($regionCode = '')
    {
		
		//connect this to the api in the future
		
		/*$languageList = Utils::makeRequest('GET', "getLanguagesByRegion/$regionCode");
		
		//change the array key from 'code' to 'key' so as to be handled correctly by custom
		foreach ( $languageList as $k=>$v )
		{
		  $languageList[$k]->key = $languageList[$k]->code;
		  unset($languageList[$k]->code);
		}*/
		
		
		if($regionCode == 1){
			$languageList = array(	
						array("id"=>1, "name"=>"English"),
						array("id"=>2, "name"=>"Spanish")
					);
		} else {
			$languageList = array(	
						array("id"=>1, "name"=>"English")
					);
		}
		
		return $languageList;
		
    }
	
    public static function getAccountList()
    {
        $items = array(
					(object) array("key"=>'admin', "name"=>"Admin"),
					(object) array("key"=>'user', "name"=>"User")
				);
		return $items;
    }
	
    public static function getRoles()
    {
        $items = array(
					(object) array("key"=>'regular', "name"=>"Regular"),
					(object) array("key"=>'editor', "name"=>"Editor"),
					(object) array("key"=>'master', "name"=>"Master")
				);
		return $items;
    }
	
    public static function getStatusList()
    {
        $items = array(
					(object) array("key"=>'1', "name"=>"Active"),
					(object) array("key"=>'0', "name"=>"Inactive")
				);
		return $items;
    }
	
    public static function getHeaderStyleList()
    {
        $items = array(
					(object) array("key"=>'1', "name"=>"Text"),
					(object) array("key"=>'2', "name"=>"Logo"),
					(object) array("key"=>'3', "name"=>"Banner"),
					(object) array("key"=>'4', "name"=>"Logo & Banner")
				);
		return $items;
    }
}
