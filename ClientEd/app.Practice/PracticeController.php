<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\LifeLearn\PracticeService as PracticeService;
use App\Libraries\LifeLearn\GeneralService as GeneralService;
use App\Libraries\LifeLearn\UserService as UserService;

class PracticeController extends Controller
{
	
	public function practices(Request $request = null){
		
		$search = $request->input('search') ? $request->input('search') : '_';
		$pageNum = $request->input('page') ? $request->input('page') : 0;
		
		$practiceList = PracticeService::getPracticeList($request, $search, $pageNum);	
				
        return view('practices', ["list" => $practiceList, 'searchTerm' => $search]);
	}
	
	
	public function practice($id){
		
		$practiceDetails = PracticeService::getPracticeDetails($id);
		
		//get the options for dropdowns	
		$accountTypeList = GeneralService::getAccountTypeList();	
		$countryList = GeneralService::getCountryList();	
		$provinceList = GeneralService::getProvinceList($practiceDetails['country']['value']);
		$regionList = GeneralService::getRegionList();	
		$colorSchemes = GeneralService::getColorSchemes();	
		$maxUsers = GeneralService::getMaxUsers();	
		$pmSoftware = GeneralService::getPMSoftware();	
		$languages = GeneralService::getLanguages($practiceDetails['region']['value']);	
		$statusList = GeneralService::getStatusList();	
		$headerStyleList = GeneralService::getHeaderStyleList();	
		
		//create the POC List for the client
		$practiceUserList = UserService::getPracticeUsersList($id, '_', 0);
		$POCList = array();
			
		foreach($practiceUserList['data']->data as $user){
			array_push($POCList, (object) array("key" => $user->id, "name"=> $user->first_name." ".$user->last_name));
		}
		
		$data = array("data" => $practiceDetails,
					  "accountTypeList" => $accountTypeList,
					  "regionList" => $regionList,
					  "countryList" => $countryList,
					  "provinceList" => $provinceList,
					  "colorSchemeList" => $colorSchemes,
					  "POCList" => $POCList,
					  "maxUsers" => $maxUsers,
					  "pmSoftware" => $pmSoftware,
					  "languages" => $languages,
					  "headerStyleList" => $headerStyleList,
					  "statusList" => $statusList
					 );
		
        return view('practice', $data);
	}
	
	public function save_practice(Request $request){		
		
		return PracticeService::savePracticeDetails($request);		
	}
	
	public function getAllProvincesByCountryCode($countryCode){
		
		$provinceList = GeneralService::getProvinceList($countryCode);	
		
		return json_encode($provinceList);
	}
	
	public function getLanguagesByRegion($egionCode){
		
		$languageList = GeneralService::getLanguageList($egionCode);
		
		return json_encode($languageList);
	}
}
