<?php
/**
 * Practice Service
 * This Library returns a list of contributors
 * @author Rishad Bhattacharjee
 */

namespace App\Libraries\LifeLearn;

use App\Libraries\LifeLearn\BaseService as BaseService;


use Illuminate\Http\Request as Request;
use GuzzleHttp\Client as Client;

class PracticeService extends BaseService
{
    

    public static function getPracticeFromIframeToken( $iframeToken )
    {
        $client = new Client(['base_uri' => getenv('CE_SERVICE')]);
        $clientEdRequest = $client->request(
            'GET',
            '/api/getClientByIframeToken/'.$iframeToken,
            [
                'headers' => [
                    'accept' => 'application/json'
                ]
            ]
        );
        $practice = json_decode($clientEdRequest->getBody()->getContents());
        return $practice;
    }


    public static function getPracticeList($request, $searchTerm, $pageNum)
    {		
		
		$user = $request->session()->get('user');
		
	 	$client = new Client(['base_uri' => getenv('CE_SERVICE')]);
        $clientEdRequest = $client->request(
            'GET',
            "api/getClientList/$searchTerm?page=$pageNum",
            [
                'headers' => [
                    'accept' => 'application/json',
					'authorization' => 'Bearer ' . $user['tokens']->access_token
                ]
            ]
        );

        $clientList = json_decode($clientEdRequest->getBody()->getContents());
		
        $tableHeaders = array("Practice Name", "Users", "Region", "PIMS");
		
		
		return array("headers"=>$tableHeaders, "data"=>$clientList);
    }


    public static function getPracticeDetails($id)
    {
		
		$practiceDetails = array('accountType'=>array('value'=>'', 'fieldName'=>'Account Type', 'required'=>true),
									'region'=>array('value'=>'', 'fieldName'=>'Region', 'required'=>true),
									'practiceName'=>array('value'=>'', 'fieldName'=>'Practice Name', 'required'=>true),
									'address'=>array('value'=>'', 'fieldName'=>'Address', 'required'=>true),
									'city'=>array('value'=>'', 'fieldName'=>'City', 'required'=>true),
									'country'=>array('value'=>'', 'fieldName'=>'Country', 'required'=>true),
									'province'=>array('value'=>'', 'fieldName'=>'Province', 'required'=>true),
									'postalCode'=>array('value'=>'', 'fieldName'=>'ZIP/Postal Code', 'required'=>true),
									'phone'=>array('value'=>'', 'fieldName'=>'Phone', 'required'=>true),
									'phoneExt'=>array('value'=>'', 'fieldName'=>'Phone Extension', 'required'=>false),
									'secondaryPhone'=>array('value'=>'', 'fieldName'=>'Secondary Phone', 'required'=>false),
									'secondaryPhoneExt'=>array('value'=>'', 'fieldName'=>'Secondary Phone Extension', 'required'=>false),
									'email'=>array('value'=>'', 'fieldName'=>'email', 'required'=>true),
									'fax'=>array('value'=>'', 'fieldName'=>'Fax', 'required'=>false),
									'pointOfContact'=>array('value'=>'', 'fieldName'=>'Point of Contact', 'required'=>false),
									'library'=>array('value'=>'', 'fieldName'=>'Library', 'required'=>false),
									'language'=>array('value'=>array(), 'fieldName'=>'Language', 'required'=>true),
									'pmSoftware'=>array('value'=>'', 'fieldName'=>'PM Software', 'required'=>true),
									'pmSoftwareOther'=>array('value'=>'', 'fieldName'=>'Other PM Software', 'required'=>false),
									'maxUsers'=>array('value'=>'', 'fieldName'=>'Max Users', 'required'=>true),
									'headerStyle'=>array('value'=>'', 'fieldName'=>'Header Style', 'required'=>false),
									'colorScheme'=>array('value'=>'', 'fieldName'=>'Color Scheme', 'required'=>false),
									'grayscale'=>array('value'=>false, 'fieldName'=>'Grayscale', 'required'=>false),
									'domain'=>array('value'=>'', 'fieldName'=>'Domain', 'required'=>false),
									'practiceLogo'=>array('value'=>'', 'fieldName'=>'Logo', 'required'=>false),
									'practiceBanner'=>array('value'=>'', 'fieldName'=>'Banner', 'required'=>false),
									'clientID'=>array('value'=>0, 'fieldName'=>'Client ID', 'required'=>false),
									'dateCreated'=>array('value'=>'', 'fieldName'=>'Date Created', 'required'=>false),
									'practiceStatus'=>array('value'=>'', 'fieldName'=>'Practice Status', 'required'=>true)
							 );
		
		
		
		if($id != "new"){		
			
			$client = new Client(['base_uri' => getenv('CE_SERVICE')]);
			$clientEdRequest = $client->request(
				'GET',
				"api/getClientDetails/$id",
				[
					'headers' => [
						'accept' => 'application/json'
					]
				]
			);
			$practiceDetail = json_decode($clientEdRequest->getBody()->getContents());

			
			$practiceDetails['accountType']['value'] = $practiceDetail->account_type_id;
			$practiceDetails['region']['value'] = $practiceDetail->region_type_id;
			$practiceDetails['practiceName']['value'] = $practiceDetail->name;
			$practiceDetails['address']['value'] = $practiceDetail->line_1;
			$practiceDetails['city']['value'] = $practiceDetail->city;
			$practiceDetails['country']['value'] = $practiceDetail->country_code;
			$practiceDetails['province']['value'] = $practiceDetail->region_code;
			$practiceDetails['postalCode']['value'] = $practiceDetail->postcode;
			
			$practiceDetails['phone']['value'] = $practiceDetail->phones[0]->phone;
			$practiceDetails['phoneExt']['value'] = $practiceDetail->phones[0]->extension;
			
			$practiceDetails['secondaryPhone']['value'] = isset($practiceDetail->phones[1]) ? $practiceDetail->phones[1]->phone : '' ;
			$practiceDetails['secondaryPhoneExt']['value'] = isset($practiceDetail->phones[1]) ? $practiceDetail->phones[1]->extension : '' ;			
			
			$practiceDetails['email']['value'] = $practiceDetail->email;
			$practiceDetails['fax']['value'] = $practiceDetail->fax;
			$practiceDetails['pointOfContact']['value'] = $practiceDetail->poc_id;
			
//			$practiceDetails['library']['value'] = $practiceDetail->fax;
			$practiceDetails['library']['value'] = array(1,3);
			
			$practiceDetails['language']['value'] = $practiceDetail->language;
//			$practiceDetails['language']['value'] = array(1,3);
			
			$practiceDetails['pmSoftware']['value'] = $practiceDetail->pm_software;
			$practiceDetails['pmSoftwareOther']['value'] = '';
			$practiceDetails['maxUsers']['value'] = $practiceDetail->max_user_id;
			$practiceDetails['headerStyle']['value'] = $practiceDetail->header_type_id;
			$practiceDetails['grayscale']['value'] = $practiceDetail->grey_scale == 0 ? false : true;
			$practiceDetails['domain']['value'] = $practiceDetail->website;
			$practiceDetails['practiceLogo']['value'] = $practiceDetail->logo;
			$practiceDetails['practiceBanner']['value'] = $practiceDetail->banner;
			$practiceDetails['clientID']['value'] = $practiceDetail->client_id;
			$practiceDetails['dateCreated']['value'] = date("M j, Y", strtotime($practiceDetail->created_at));
			$practiceDetails['practiceStatus']['value'] = $practiceDetail->active;
		} 
		
		return $practiceDetails;
    }
	
	public static function savePracticeDetails($request){
		
		$user = $request->session()->get('user');
		
		$data = $request->request->all();
				
		$client = new Client(['base_uri' => getenv('CE_SERVICE')]);
        $clientEdRequest = $client->request(
            'POST',
            "api/saveClientDetails",
            [
                'headers' => [
                    'accept' => 'application/json',
					'authorization' => 'Bearer ' . $user['tokens']->access_token
                ],
				'form_params' => $data, 
            ]
        );
		
		$practiceDetails = (array) json_decode($clientEdRequest->getBody()->getContents());
				
		return $practiceDetails;
	}

}